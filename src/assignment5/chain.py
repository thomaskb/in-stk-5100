import numpy as np
import gym
from gym import spaces
from gym.utils import seeding


# Here bandit problems are sampled from a Beta distribution
class Chain(gym.Env):
    def __init__(self, states=5, delta=0.1, epsilon=0.1):
        self.n_states = states
        self.n_actions = 2
        self.r_dist = np.zeros(states)
        self.r_dist[0] = epsilon
        self.r_dist[self.n_states - 1] = 1
        self.action_space = spaces.Discrete(self.n_actions)
        self.observation_space = spaces.Discrete(self.n_states)
        self.delta = delta
        self.epsilon = epsilon
        self.state = 0  #
        self._seed()
        self.reset()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):
        assert self.action_space.contains(action)
        done = False
        # I find it unintuitive when the reward is given for the last state and not the next state
        #reward = np.random.binomial(1, self.r_dist[self.state])

        move = action
        # swap the move with delta probability
        if np.random.uniform() < self.delta:
            move = 1 - action
        if move == 0:
            self.state = 0
        else:
            self.state += 1
            if self.state > self.n_states - 1:
                self.state = self.n_states - 1
        # I put it here, because I found it more intuitive if r(s,a,s') = r(s'), instead of r(s,a) = r(s)
        reward = np.random.binomial(1, self.r_dist[self.state])
        return self.state, reward, done

    def reset(self):
        self.state = 0
        return 0

    def render(self, mode='human', close=False):
        pass

