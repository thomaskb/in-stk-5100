import numpy as np
import src.assignment5.chain as chain
import matplotlib.pyplot as plt


""" 
Setting alpha: I feel like the initial value for alpha is not that relevant. The decay is a bit tricky. In the chain environment it doesn't make a huge difference though.
Setting epsilon for exploration and its decay is more difficult and relevant here I think.
In particular, when there are many states it gets difficult (also because there is this delta probability of going to state 0). Very slow decay kind off works though.
And for many states, e.g. 50, well, there's is only a very small chance of ever seeing state 50 even with high exploration rate.

We clearly expect replaying past observations to speed up Q-Learning, which it does. 
Here, I sample from past observations and then use the consecutive K steps and update the Q-matrix with those again. 
I used sequences of past observations and not single samples because I thought it makes sense for the chain environment(?, IDK).

Replaying past observations doesn't really make sense for SARSA as it is on-policy, i.e. the observation at some past time step was generated by the policy at that time.
So, it shouldn't help, but possibly hinder learning. However, I feel like in this setting it could help nevertheless as the policy/action space is simple. But IDK; curious
what the experiments of other people showed.

"""

n_states = 5
n_actions = 2


class QLearning:
    def __init__(self, alpha, gamma, epsilon):
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        self.n_actions = n_actions
        self.n_states = n_states
        self.q_matrix = np.zeros([self.n_states, self.n_actions])
        self.rewards = []  # to calculate avg reward

    # state = chain.state
    # epsilon greedy policy
    def act(self, state):
        if np.random.binomial(1, self.epsilon) == 1:
            return np.random.choice(n_actions, 1, 0.5)[0]
        else:
            return np.argmax(self.q_matrix[state])

    # TD update: q(s,a) = q(s,a) + alpha * (r(s,a,s') + gamma * max_a q(s',a) - q(s,a))
    def update(self, state, action, next_state, reward):
        self.q_matrix[state, action] += self.alpha * (reward + self.gamma * max(self.q_matrix[next_state][:]) - self.q_matrix[state][action])
        self.rewards.append(reward)


chain = chain.Chain(n_states)
alpha, gamma, epsilon = 0.5, 0.9, 0.4
q_learning = QLearning(alpha, gamma, epsilon)
chain.reset()
avg_rewards = []
alpha_evol = []
exploration_evol = []
alpha_decay = 0.9995
epsilon_decay = 0.9995
T = 5000
for i in range(T):
    state = chain.state
    # get action
    action = q_learning.act(state)
    # make move
    next_state, reward, done = chain.step(action)
    # update q-values
    q_learning.update(state, reward, next_state, reward)
    # update learning rate and epsilon exploration
    alpha_evol.append(alpha)
    exploration_evol.append(epsilon)
    #alpha = alpha / (alpha + (i+1)**0.55)
    alpha *= alpha_decay
    epsilon *= epsilon_decay
    # avg reward
    avg_rewards.append(sum(q_learning.rewards)/(i+1))
    # prints
    print('---')
    print('Epsilon', epsilon)
    print('Time step', i)
    print('State:', state)
    print('Action:', action)
    print('Avg reward:', avg_rewards[i])
    print(q_learning.q_matrix)

# plt.plot(avg_rewards, label="Avg reward")
# plt.plot(alpha_evol, label="Learning rate")
# plt.plot(exploration_evol, label="Exploration rate")
# plt.legend()
# plt.title("Q-Learning: Chain Env w/ " + str(n_states) + " states")
# plt.savefig("Q_Learning_Chain.pdf")



class QLearningWithReplay:
    def __init__(self, alpha, gamma, epsilon):
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        self.n_actions = n_actions
        self.n_states = n_states
        self.q_matrix = np.zeros([self.n_states, self.n_actions])
        self.rewards = []  # to calculate avg reward
        self.history = []  # containing past steps

    # state = chain.state
    # epsilon greedy policy
    def act(self, state):
        if np.random.binomial(1, self.epsilon) == 1:
            return np.random.choice(n_actions, 1, 0.5)[0]
        else:
            return np.argmax(self.q_matrix[state])

    # TD update: q(s,a) = q(s,a) + alpha * (r(s,a,s') + gamma * max_a q(s',a) - q(s,a))
    def update(self, state, action, next_state, reward):
        self.q_matrix[state, action] += self.alpha * (reward + self.gamma * max(self.q_matrix[next_state][:]) - self.q_matrix[state][action])
        self.rewards.append(reward)

        # I guess replay makes sense after a few steps only
        if len(self.history) > 100:
            K = 5
            # get a random time step
            random_observation = np.random.choice(len(self.history) - K)
            # get the history of that time step and the consecutive X times steps (I think this makes sense for the chain?)
            # we are replaying a portion of the history
            for hist in [self.history[random_observation + j] for j in range(K)]:
                self.q_matrix[hist[0], hist[1]] += self.alpha * (hist[2] + self.gamma * max(self.q_matrix[hist[3]][:]) - self.q_matrix[hist[0]][hist[1]])


alpha, gamma, epsilon = 0.5, 0.9, 0.4
q_learning_with_replays = QLearningWithReplay(alpha, gamma, epsilon)
avg_rewards_replays = []
alpha_evol = []
exploration_evol = []
alpha_decay = 0.9995
epsilon_decay = 0.9995
chain.reset()
for i in range(T):
    state = chain.state
    # get action
    action = q_learning.act(state)
    # make move
    next_state, reward, done = chain.step(action)
    # save history
    q_learning_with_replays.history.append([state, action, reward, next_state])
    # update q-values
    q_learning_with_replays.update(state, reward, next_state, reward)
    # update learning rate and epsilon exploration
    alpha_evol.append(alpha)
    exploration_evol.append(epsilon)
    #alpha = alpha / (alpha + (i+1)**0.55)
    alpha *= alpha_decay
    epsilon *= epsilon_decay
    # avg reward
    avg_rewards_replays.append(sum(q_learning_with_replays.rewards)/(i+1))
    # prints
    print('---')
    print('Epsilon', epsilon)
    print('Time step', i)
    print('State:', state)
    print('Action:', action)
    print('Avg reward:', avg_rewards_replays[i])
    print(q_learning_with_replays.q_matrix)


plt.plot(avg_rewards, label="Avg reward Q-Learning")
plt.plot(avg_rewards_replays, label="Avg reward w/ Replays")
plt.plot(alpha_evol, label="Learning rate")
plt.plot(exploration_evol, label="Exploration rate")
plt.legend()
plt.title("Q_Learning: Chain Env w/ " + str(n_states) + " states")
plt.savefig("Q_Learning_Chain_env.pdf")
