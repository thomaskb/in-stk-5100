import numpy as np
import gym
import gym_bandits


class AverageBanditAlgorithm:
    def __init__(self, n_actions):
        self.n_actions = n_actions
        self.total_reward = np.ones(n_actions)
        self.n_pulls = np.ones(n_actions)

    def act(self):
        return np.argmax(self.total_reward / self.n_pulls)

    def update(self, action, reward):
        self.total_reward[action] += reward
        self.n_pulls[action] += 1


class BayesOptimalBernoulliBandit:
    def __init__(self, n_actions, horizon):
        self.n_actions = n_actions
        self.horizon = horizon  # maximum steps to look ahead
        ## Beta distribution parameters
        self.alpha = np.ones(n_actions)
        self.beta = np.ones(n_actions)


    # I'll quickly do the math here;
    # E(r_t | xi_t, a_t) = alpha_t / alpha_t + beta_t (the estimate of the Bern parameter), also equal to xi_t(r_t = 1 | a_t)
    # Need P(xi_{t+1} | xi_t, a_t) U(xi_{t+1})
    # That's basically something like xi_t(r_t = 1) (alpha_t + 1, beta_t) + xi_t(r_t = 0) (alpha_t, beta_t + 1) (all for the arm a_t)

    # call with alpha_rec = self.alpha, ...
    # call with n = horizon
    # should calculate U^*(xi_{t+1})
    def recurse_utility(self, n, alpha_rec, beta_rec):
        if n < 1:
            return 0
        alpha = alpha_rec.copy()
        beta = beta_rec.copy()
        util = np.zeros(self.n_actions)
        expected_reward = alpha / (alpha + beta)
        # adding expected_reward
        util += expected_reward
        # two possible xi_{t+1} for each action
        for a in range(self.n_actions):
            new_alpha = alpha.copy()
            new_alpha[a] += 1
            new_beta = beta.copy()
            new_beta[a] += 1
            util[a] += expected_reward[a] * self.recurse_utility(n-1, new_alpha, beta)
            util[a] += (1-expected_reward)[a] * self.recurse_utility(n-1, alpha, new_beta)
        return max(util)


    def calculate_utility(self, action):
        util = 0
        expected_reward = self.alpha[action] / (self.alpha[action] + self.beta[action])
        util += expected_reward
        # ugly but works: define the new parameters
        new_alpha = self.alpha.copy()
        new_alpha[action] += 1
        util += expected_reward * self.recurse_utility(self.horizon, new_alpha, self.beta)
        new_beta = self.beta.copy()
        new_beta[action] += 1
        util += (1-expected_reward) * self.recurse_utility(self.horizon, self.alpha, new_beta)
        return util

    def act(self):
        # Fill in
        U = np.zeros(self.n_actions)
        for i in range(self.n_actions):
            U[i] = self.calculate_utility(i)
        return np.argmax(U)

    def update(self, action, reward):
        self.alpha[action] += reward
        self.beta[action] += (1 - reward)


# Set up the bandit environment
env = gym.make("BernoulliBandit-v0")


# AverageBanditAlgorithm
avg_alg = AverageBanditAlgorithm(env.action_space.n)

# BayesOptimalBernoulliAlgorithm
K = 2  # Horizon
bob_alg = BayesOptimalBernoulliBandit(env.action_space.n, K)


# Bayes-optimal run
env.reset()
total_rewards = []
iterations = 4
episode_sizes = [3, 10, 25, 50]
for episode_length in episode_sizes:
    total_reward = 0
    for iteration in range(iterations):
        env.reset()
        # resetting algorithm
        bob_alg.alpha = np.ones(bob_alg.n_actions)
        bob_alg.beta = np.ones(bob_alg.n_actions)
        for i_episode in range(episode_length):
            #observation = env.reset()
            for t in range(1):
                env.render()
                action = bob_alg.act()
                observation, reward, done, info = env.step(action)
                if episode_length - i_episode > bob_alg.horizon:
                    bob_alg.update(action, reward)
                #print("A:, R:", action, reward)
                total_reward += reward
                if done:
                    break
        total_rewards.append(total_reward / iterations)
    print("Episode length:", episode_length, "   BayesOptAlg total reward: ", total_reward/iterations)


# AverageBanditAlg run
env.reset()
total_rewards_avg = []
for episode_length in episode_sizes:
    total_reward = 0
    for iteration in range(iterations):
        env.reset()
        # resetting algorithm
        avg_alg.total_reward = np.ones(avg_alg.n_actions)
        avg_alg.n_pulls = np.ones(avg_alg.n_actions)
        for i_episode in range(episode_length):
            #observation = env.reset()
            for t in range(1):
                env.render()
                action = avg_alg.act()
                # action = env.action_space.sample()
                observation, reward, done, info = env.step(action)
                avg_alg.update(action, reward)
                #print("A:, R:", action, reward)
                total_reward += reward
                if done:
                    break
    total_rewards_avg.append(total_reward / iterations)
    print("Episode length", episode_length, "   AvgAlg total reward: ", total_reward/iterations)
env.close()


# Test run:
# Episode length: 3    BayesOptAlg total reward:  2.5
# Episode length: 10    BayesOptAlg total reward:  9.0
# Episode length: 25    BayesOptAlg total reward:  22.0
# Episode length: 50    BayesOptAlg total reward:  43.75
# Episode length 3    AvgAlg total reward:  2.5
# Episode length 10    AvgAlg total reward:  7.75
# Episode length 25    AvgAlg total reward:  17.75
# Episode length 50    AvgAlg total reward:  38.75
